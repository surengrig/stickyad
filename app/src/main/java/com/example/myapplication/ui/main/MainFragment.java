package com.example.myapplication.ui.main;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.ViewParent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplication.R;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container,

                             @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.main_fragment, container, false);
        final WebView adWebView = view.findViewById(R.id.adWebView);
        final ViewGroup stickyAdContainer = view.findViewById(R.id.stickyAdContainer);
        final ViewGroup adContainer = view.findViewById(R.id.adContainer);
        final ScrollView containerScrollView = view.findViewById(R.id.containerScrollView);

        adWebView.loadUrl("https://creatives-cached.yoc.com/cdn/helper/creatives/image-creative-320x50.html");

        // add an empty space with height of ad on the bottom of the scrollview
        adWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                adWebView.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        View fillAdSpace = new View(getContext());
                        int height = adWebView.getHeight();

                        fillAdSpace.setLayoutParams(new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        height
                                )
                        );

                        adContainer.addView(fillAdSpace);

                    }
                }, 300);
            }
        });

        // handles the ad transformation to sticky add
        containerScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                final Rect rect = new Rect();
                containerScrollView.getDrawingRect(rect);
                boolean isVisible = isViewVisible(adWebView, rect);
                if (!isVisible) {
                    ((ViewManager) adWebView.getParent()).removeView(adWebView);


                    Rect containerRect = new Rect();
                    stickyAdContainer.getDrawingRect(containerRect);
                    stickyAdContainer.addView(adWebView);

                    adWebView.setTranslationY(adWebView.getHeight() + containerRect.height());
                    adWebView.animate()
                            .setDuration(400)
                            .translationY(-adWebView.getHeight() + containerRect.height())
                            .alpha(1.0f);

                    containerScrollView.setOnScrollChangeListener(null);

                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
    }


    /**
     * Determines whether the view is partially or fully visible
     */
    private boolean isViewVisible(View view, Rect rect) {
        float top = view.getY();
        ViewParent parent = view.getParent();
        while (!(parent instanceof ScrollView)) {
            top += ((View) parent).getY();
            parent = parent.getParent();
        }
        return rect.top < top + view.getHeight();
    }
}
